let modalContainer = document.getElementById("container-modal");
let containerLogin = document.getElementById("loginContainer");

function createModal(picture) {
  let modal = document.createElement("div");
  let image = document.createElement("img");
  let close = document.createElement("p");

  modal.className = "modal-image";
  modal.id = "modal-image";
  image.src = picture;
  close.innerText = "x";

  close.onclick = function() {
    let modal = document.getElementById("modal-image");
    modal.remove();
  };

  modal.appendChild(image);
  modal.appendChild(close);
  modalContainer.appendChild(modal);
}

function createLogin() {
  let modalLogin = document.createElement("div");
  let login = document.createElement("div");
  let titleLogin = document.createElement("h3");
  let formLogin = document.createElement("form");
  let emailName = document.createElement("p");
  let emailInput = document.createElement("input");
  let passwordName = document.createElement("p");
  let passwordInput = document.createElement("input");
  let buttonContainer = document.createElement("div");
  let button = document.createElement("button");

  modalLogin.className = "modal-login";
  modalLogin.id = "modal-login";
  login.className = "login";

  titleLogin.innerText = "Login";
  emailName.innerText = "E-mail:";
  emailInput.placeholder = "name@email.com";
  passwordName.innerText = "Password:";
  passwordInput.type = "password";
  button.innerText = "Login";

  buttonContainer.appendChild(button);
  formLogin.appendChild(emailName);
  formLogin.appendChild(emailInput);
  formLogin.appendChild(passwordName);
  formLogin.appendChild(passwordInput);
  formLogin.appendChild(buttonContainer);

  login.appendChild(titleLogin);
  login.appendChild(formLogin);

  modalLogin.appendChild(login);

  let register = document.createElement("div");
  let titleRegister = document.createElement("h3");
  let formRegister = document.createElement("form");
  let userName = document.createElement("p");
  let userNameInput = document.createElement("input");
  let emailNameRegister = document.createElement("p");
  let emailInputRegister = document.createElement("input");
  let passwordNameRegister = document.createElement("p");
  let passwordInputRegister = document.createElement("input");
  let confirmPasswordName = document.createElement("p");
  let confirmPasswordInput = document.createElement("input");
  let buttonContainerRegister = document.createElement("div");
  let buttonRegister = document.createElement("button");

  register.className = "register";

  titleRegister.innerText = "Register";
  userName.innerText = "Name:";
  userNameInput.placeholder = "Your name";
  emailNameRegister.innerText = "E-mail:";
  emailInputRegister.placeholder = "name@email.com";
  passwordNameRegister.innerText = "Password:";
  passwordInputRegister.type = "password";
  confirmPasswordName.innerText = "Confirm password:";
  confirmPasswordInput.type = "password";
  buttonRegister.innerText = "Register";

  buttonContainerRegister.appendChild(buttonRegister);

  formRegister.appendChild(userName);
  formRegister.appendChild(userNameInput);
  formRegister.appendChild(emailNameRegister);
  formRegister.appendChild(emailInputRegister);
  formRegister.appendChild(passwordNameRegister);
  formRegister.appendChild(passwordInputRegister);
  formRegister.appendChild(confirmPasswordName);
  formRegister.appendChild(confirmPasswordInput);
  formRegister.appendChild(buttonContainerRegister);

  register.appendChild(titleRegister);
  register.appendChild(formRegister);

  let close = document.createElement("p");

  close.className = "closeLogin";
  close.innerText = "x";

  modalLogin.appendChild(register);
  modalLogin.appendChild(close);
  containerLogin.appendChild(modalLogin);

  close.onclick = function() {
    let modalLoginClose = document.getElementById("modal-login");
    modalLoginClose.remove();
  };
}
