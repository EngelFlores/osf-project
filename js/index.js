const arrivalsSection = document.getElementById("arrivals-section");
const sellersSection = document.getElementById("best-sellers-section");
const textBoard = document.getElementById("big-board");
const twitterWidget = document.getElementById("widget");

function dropDown() {
  document.getElementById("dropdown").classList.toggle("show");
}

window.onclick = function(event) {
  if (!event.target.matches(".header-search")) {
    let dropdowns = document.getElementsByClassName("search-list");
    let i;
    for (i = 0; i < dropdowns.length; i++) {
      let openDropdown = dropdowns[i];
      if (openDropdown.classList.contains("show")) {
        openDropdown.classList.remove("show");
      }
    }
  }
};

function arrivalsBuilder(product) {
  let imageContainer = document.createElement("div");
  let productInfo = document.createElement("div");
  let top = document.createElement("div");
  let bottom = document.createElement("div");
  let price = document.createElement("p");
  let name = document.createElement("p");
  let iconsContainer = document.createElement("div");
  let eye = document.createElement("i");
  let star = document.createElement("i");
  let external = document.createElement("i");
  let cart = document.createElement("i");

  imageContainer.className = "product-container";
  productInfo.className = "product-info";
  top.className = "top-info";
  bottom.className = "bottom-info";
  iconsContainer.className = "icons-hover";
  eye.className = "far fa-eye icon-container";
  star.className = "far fa-star icon-container";
  external.className = "fas fa-external-link-alt icon-container";
  cart.className = "fas fa-cart-arrow-down icon-container";

  imageContainer.style.backgroundImage = `url(${product.src})`;
  product.info && (top.innerText = product.info);
  product.info && (top.style.backgroundColor = "#f27e4b");

  price.innerText = product.price;
  name.innerText = product.name;

  iconsContainer.appendChild(eye);
  iconsContainer.appendChild(star);
  iconsContainer.appendChild(external);
  iconsContainer.appendChild(cart);
  imageContainer.appendChild(productInfo);
  productInfo.appendChild(top);
  productInfo.appendChild(bottom);
  bottom.appendChild(price);
  bottom.appendChild(name);
  bottom.appendChild(iconsContainer);

  imageContainer.onmouseover = function() {
    name.style.display = "none";
    iconsContainer.style.display = "block";
  };
  imageContainer.onmouseout = function() {
    name.style.display = "block";
    iconsContainer.style.display = "none";
  };

  return imageContainer;
}

(function mapingArrivals() {
  arrivalsList.forEach(createArrivals);
})();

function createArrivals(product) {
  let imageGenerated = arrivalsBuilder(product);
  arrivalsSection.appendChild(imageGenerated);
}

function sellersBuilder(product) {
  let imageContainer = document.createElement("div");
  let productInfo = document.createElement("div");
  let top = document.createElement("div");
  let bottom = document.createElement("div");
  let price = document.createElement("p");
  let name = document.createElement("p");
  let iconsContainer = document.createElement("div");
  let eye = document.createElement("i");
  let star = document.createElement("i");
  let external = document.createElement("i");
  let cart = document.createElement("i");

  imageContainer.className = "product-container";
  productInfo.className = "product-info";
  top.className = "top-info";
  bottom.className = "bottom-info";
  iconsContainer.className = "icons-hover";
  eye.className = "far fa-eye icon-container";
  star.className = "far fa-star icon-container";
  external.className = "fas fa-external-link-alt icon-container";
  cart.className = "fas fa-cart-arrow-down icon-container";

  imageContainer.style.backgroundImage = `url(${product.src})`;
  product.info && (top.innerText = product.info);
  product.info && (top.style.backgroundColor = "#f27e4b");

  price.innerText = product.price;
  name.innerText = product.name;

  iconsContainer.appendChild(eye);
  iconsContainer.appendChild(star);
  iconsContainer.appendChild(external);
  iconsContainer.appendChild(cart);
  imageContainer.appendChild(productInfo);
  productInfo.appendChild(top);
  productInfo.appendChild(bottom);
  bottom.appendChild(price);
  bottom.appendChild(name);
  bottom.appendChild(iconsContainer);

  imageContainer.onmouseover = function() {
    name.style.display = "none";
    iconsContainer.style.display = "block";
  };
  imageContainer.onmouseout = function() {
    name.style.display = "block";
    iconsContainer.style.display = "none";
  };

  return imageContainer;
}

(function mapingSellers() {
  bestSellersList.forEach(createSellers);
})();

function createSellers(product) {
  let imageGenerated = sellersBuilder(product);
  sellersSection.appendChild(imageGenerated);
}

function randomText() {
  for (let index = 0; index < 2; index++) {
    let textId = Math.floor(Math.random() * blogNews.length);
    let lastNumber = "";
    let isEqual = textId === lastNumber;
    isEqual && (textId = Math.floor(Math.random() * blogNews.length));
    let textContent = blogNews[textId];
    createText(textContent);
  }
}

function clearText() {
  for (let index = 0; index < 2; index++) {
    let removeDiv = document.getElementById("news");
    removeDiv.remove();
  }
  randomText();
}

randomText();

window.setInterval(clearText, 5000);

function createText(phrase) {
  let textGenerated = textBuilder(phrase);
  textBoard.appendChild(textGenerated);
}

function textBuilder(phrase) {
  let news = document.createElement("div");
  let text = document.createElement("div");
  let newsDate = document.createElement("p");
  let title = document.createElement("h4");
  let subtitle = document.createElement("p");

  news.className = "news";
  news.id = "news";
  newsDate.className = "news-date";
  text.className = "text";

  newsDate.innerText = phrase.date;
  title.innerText = phrase.title;
  subtitle.innerText = phrase.subtitle;

  text.appendChild(title);
  text.appendChild(subtitle);
  news.appendChild(newsDate);
  news.appendChild(text);

  return news;
}

function randomComment() {
  for (let index = 0; index < 2; index++) {
    let textId = Math.floor(Math.random() * twitter.length);
    let lastNumber = "";
    let isEqual = textId === lastNumber;
    isEqual && (textId = Math.floor(Math.random() * twitter.length));
    let textContent = twitter[textId];
    createComment(textContent);
  }
}

function clearTwitter() {
  for (let index = 0; index < 2; index++) {
    let removeDiv = document.getElementById("twitter");
    removeDiv.remove();
  }
  randomComment();
}

randomComment();

window.setInterval(clearTwitter, 5000);

function createComment(expression) {
  let commentGenerated = twitterBuilder(expression);
  twitterWidget.appendChild(commentGenerated);
}

function twitterBuilder(expression) {
  let widget = document.createElement("div");
  let text = document.createElement("p");
  let date = document.createElement("p");
  let user = document.createElement("span");
  let comment = document.createElement("span");

  widget.id = "twitter";
  user.innerText = expression.user;
  comment.innerText = expression.comment;
  text.innerHTML += user.outerHTML + comment.outerHTML;
  date.innerText = expression.date;

  widget.appendChild(text);
  widget.appendChild(date);

  return widget;
}

function changeList() {
  arrivalsSection.style.flexDirection = "column";
  sellersSection.style.flexDirection = "column";
}

function changeGrid() {
  arrivalsSection.style.flexDirection = "row";
  sellersSection.style.flexDirection = "row";
}
